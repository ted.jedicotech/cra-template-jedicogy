import { Navigate, useOutlet } from "react-router-dom";
import { useAuth } from "../core/authentication/authProvider";

export const HomeLayout = () => {
  const { token } = useAuth();
  const outlet = useOutlet();

  if (token) {
    return <Navigate to="/dashboard" />;
  } 

  return (
    <div>
      {outlet}
    </div>
  );
};
