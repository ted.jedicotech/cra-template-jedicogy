import { Navigate, useOutlet } from "react-router-dom";
import { useAuth } from "../core/authentication/authProvider";
import Side from "../core/layout/Side/Side";

export const ProtectedLayout = () => {
  const { token } = useAuth();
  const outlet = useOutlet();

  if (!token) {
    return <Navigate to="/login" />;
  }

  return (
    <div>
      {outlet}
    </div>
  );
};
