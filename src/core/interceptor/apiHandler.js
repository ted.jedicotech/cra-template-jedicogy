import axios from "axios";
// import {useAuth} from "../authentication/authProvider" 
import { useNavigate } from "react-router-dom";
const apiHandler = axios.create({
  baseURL: 'https://1ed212af-05d6-4517-88b6-1276d352d6f9.mock.pstmn.io/',
  headers: {
    Accept: "application/json",
  },
});

apiHandler.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem("token");
    console.log('aaaaa',token)
    if (token) {
      config.headers["Authorization"] = "Bearer " + token;
    }
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

apiHandler.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error.response.status === 401) {
      // const { logout } = useAuth();
      // logout();
    } else if (error.response.status === 404) {
      console.log('4040404004',error.response.status)
      const navigate = useNavigate();
      navigate("/empty", { replace: true });
    }
    return Promise.reject(error);
  }
);



export default apiHandler;