import { createContext, useContext, useMemo } from "react";
import { useNavigate } from "react-router-dom";
import { useLocalStorage } from "../service/useLocalStorage";
import axios from 'axios'

const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [token, setToken] = useLocalStorage("token", null);
  const navigate = useNavigate();
  const loginAPI = 'https://1ed212af-05d6-4517-88b6-1276d352d6f9.mock.pstmn.io/api/auth/login';
  const login = async (loginData) => {
    axios.post(loginAPI, loginData).then((response) => {
      const resData = response.data;
      const token = resData.token;
      if (!token) {
          alert('Unable to login. Please try after some time.');
          return;
      }
      setToken(token);
      navigate("/dashboard", { replace: true });
  }).catch((error) => {
      alert("Oops! Some error occured.");
  });

  };

  const logout = () => {
    setToken(null);
    navigate("/login", { replace: true });
  };

  const value = useMemo(
    () => ({
      token,
      login,
      logout
    }),
    [token]
  );


  


  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
};

export const useAuth = () => {
  return useContext(AuthContext);
};
