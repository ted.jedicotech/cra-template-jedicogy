import React from 'react';
import PropTypes from 'prop-types';
import { FooterWrapper } from './Footer.styled';

export const Footer = (props) => {

   return (
       <div className="layout-footer">
           <img src={props.layoutColorMode === 'light' ? 'assets/layout/images/logo-dark.svg' : 'assets/layout/images/logo-white.svg'} alt="Logo" height="20" className="mr-2" />
           by
           <span className="font-medium ml-6">PrimeReact</span>
       </div>
   );
}


Footer.propTypes = {};

Footer.defaultProps = {};

export default Footer;
