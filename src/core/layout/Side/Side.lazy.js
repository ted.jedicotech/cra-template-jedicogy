import React, { lazy, Suspense } from 'react';

const LazySide = lazy(() => import('./Side'));

const Side = props => (
  <Suspense fallback={null}>
    <LazySide {...props} />
  </Suspense>
);

export default Side;
