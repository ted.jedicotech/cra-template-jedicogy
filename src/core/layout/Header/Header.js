import { TabMenu } from 'primereact/tabmenu';
import { useNavigate } from "react-router-dom";
import { classNames } from 'primereact/utils';
import React from 'react';
import { Link } from 'react-router-dom';
import { useAuth } from '../../authentication/authProvider'


export const Header = (props) => {
    const { logout } = useAuth();

    const { token } = useAuth();

    const btn = (event) => {
        logout()
    };


    const LogoutCheck = (event) => {
        if(token){
            return(
                <li>
                <button className="p-link layout-topbar-button" onClick={btn}>
                    <i className="pi pi-sign-out" />
                    <span>Logout</span>
                </button>
            </li>
            );
        } 
    };


    return (
        <div className="layout-topbar">
            <Link to="/" className="layout-topbar-logo">
                <img src={props.layoutColorMode === 'light' ? 'assets/layout/images/logo-dark.svg' : 'assets/layout/images/logo-white.svg'} alt="logo" />
                <span>SAKAI</span>
            </Link>

            <button type="button" className="p-link  layout-menu-button layout-topbar-button" onClick={props.onToggleMenuClick}>
                <i className="pi pi-bars" />
            </button>

            <button type="button" className="p-link layout-topbar-menu-button layout-topbar-button" onClick={props.onMobileTopbarMenuClick}>
                <i className="pi pi-ellipsis-v" />
            </button>

            <ul className={classNames("layout-topbar-menu lg:flex origin-top", { 'layout-topbar-menu-mobile-active': props.mobileTopbarMenuActive })}>
                <li>
                    <button className="p-link layout-topbar-button" onClick={props.onMobileSubTopbarMenuClick}>
                        <i className="pi pi-calendar" />
                        <span>Events</span>
                    </button>
                </li>
                <li>
                    <button className="p-link layout-topbar-button" onClick={props.onMobileSubTopbarMenuClick}>
                        <i className="pi pi-cog" />
                        <span>Settings</span>
                    </button>
                </li>
                <li>
                    <button className="p-link layout-topbar-button" onClick={props.onMobileSubTopbarMenuClick}>
                        <i className="pi pi-user" />
                        <span>Profile</span>
                    </button>
                </li>
                <LogoutCheck/>
 
            </ul>
        </div>
    );
}

Header.displayName = 'Header';

Header.propTypes = {};

Header.defaultProps = {};

export default Header;
