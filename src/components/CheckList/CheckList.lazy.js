import React, { lazy, Suspense } from 'react';

const LazyCheckList = lazy(() => import('./CheckList'));

const CheckList = props => (
  <Suspense fallback={null}>
    <LazyCheckList {...props} />
  </Suspense>
);

export default CheckList;
