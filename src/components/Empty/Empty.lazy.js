import React, { lazy, Suspense } from 'react';

const LazyEmpty = lazy(() => import('./Empty'));

const Empty = props => (
  <Suspense fallback={null}>
    <LazyEmpty {...props} />
  </Suspense>
);

export default Empty;
