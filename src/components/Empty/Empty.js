import React from 'react';
import PropTypes from 'prop-types';
import { EmptyWrapper } from './Empty.styled';

const Empty = () => (
   <div className="grid">
   <div className="col-12">
       <div className="card">
           <h5>Empty Page</h5>
           <p>Use this page to start from scratch and place your custom content.</p>
       </div>
   </div>
</div>
);

Empty.propTypes = {};

Empty.defaultProps = {};

export default Empty;
