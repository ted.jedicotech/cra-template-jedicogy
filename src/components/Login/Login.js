import * as React from "react";
import { Link as RouterLink } from "react-router-dom";
import { useAuth } from "../../core/authentication/authProvider";
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { Checkbox } from 'primereact/checkbox';

const Login = () => {
   const { login } = useAuth();

   const handleSubmit = (event) => {
      login({
         email: "email",
         password: "password"
      });
   };


   return (
      // <button onClick={handleSubmit}>Login In</button>
      <div className="surface-card p-4 shadow-2 border-round w-full lg:w-6">
         <div className="text-center mb-5">
            <img src="assets/layout/images/blocks/logos/hyper.svg" alt="hyper" height="50" className="mb-3" />
            <div className="text-900 text-3xl font-medium mb-3">Welcome Back</div>
            <span className="text-600 font-medium line-height-3">Don't have an account?</span>
            <a className="font-medium no-underline ml-2 text-blue-500 cursor-pointer">Create today!</a>
         </div>
         <div>
            <label htmlFor="email1" className="block text-900 font-medium mb-2">Email</label>
            <InputText id="email1" type="text" className="w-full mb-3" />

            <label htmlFor="password1" className="block text-900 font-medium mb-2">Password</label>
            <InputText id="password1" type="password" className="w-full mb-3" />

            <div className="flex align-items-center justify-content-between mb-6">
               <a className="font-medium no-underline ml-2 text-blue-500 text-right cursor-pointer">Forgot password?</a>
            </div>

            <Button onClick={handleSubmit} label="Sign In" icon="pi pi-user" className="w-full" />
         </div>
      </div>
   );
};

export default Login;