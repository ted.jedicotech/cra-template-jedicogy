import React from 'react';
import PropTypes from 'prop-types';
import { TodoListWrapper } from './TodoList.styled';
import apiHandler from '../../core/interceptor/apiHandler';

const TodoList = (prop) => {
   const getData = async () => {
      await apiHandler
         .get(`/path`)
         .then((response) => {
            //do something
            console.log('response',response)
         }).catch((error) => {
            console.log('error',error)
         })
   };
   return (
         <button onClick={getData}>api Test</button>
   )

};

TodoList.propTypes = {};

TodoList.defaultProps = {};

export default TodoList;



